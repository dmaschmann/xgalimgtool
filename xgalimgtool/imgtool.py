# -*- coding: utf-8 -*-

from xgaltool.basic_attributes import PhysicalConstants, download_file

import numpy as np
import os
from pathlib import Path

from astropy.io import fits
from astropy import units as u
from astropy.coordinates import SkyCoord

import matplotlib.image as mpimg
import matplotlib.pyplot as plt

class ImgTool(PhysicalConstants):

    def __init__(self, ra, dec, data_path=None):
        r"""
        class to handle image data
        """
        super().__init__()

        self.ra = ra
        self.dec = dec
        # check if data_path is correct and add to attributes
        if data_path is None:  # use short cut
            data_path = Path.home() / 'data'
        if os.path.isdir(Path(data_path)):
            self.data_path = Path(data_path)
        else:
            raise OSError('The data path %s is not correct' % data_path)

    def get_image_path(self):
        """
        creating path to image storage
        """
        c = SkyCoord(ra=self.ra*u.degree, dec=self.dec*u.degree)
        ra_coords = c.ra.hms
        dec_coords = c.dec.dms
        d = dec_coords.d
        if d > 0:
            dec_h_str = '+%02i' % d
        else:
            if d == 0:
                if dec_coords.m > 0:
                    dec_h_str = '+%02i' % d
                else:
                    if dec_coords.m == 0:
                        if dec_coords.s >= 0:
                            dec_h_str = '+%02i' % d
                        else:
                            dec_h_str = '-%02i' % abs(d)
                    else:
                        dec_h_str = '-%02i' % abs(d)
            else:
                dec_h_str = '-%02i' % abs(d)

        image_path = self.data_path / Path('images/ra_%i_%i_%.2f_dec_%s_%i_%.2f/' %
                                           (ra_coords.h, ra_coords.m, ra_coords.s, dec_h_str, dec_coords.m,
                                            dec_coords.s))
        # make sure the path exists
        if not os.path.isdir(image_path):
            os.makedirs(image_path)
        return image_path

    def get_legacy_survey_img_url(self, layer='ls-dr9', bands=None, file_format='jpg', height=70, width=70, scale=262):
        """
        compose URL according to https://www.legacysurvey.org/dr9/description/
        The projections for the filters g,r,z are identical, with a pixel scale of 0.262″/pix
        for example for a 60" x 60" image we can use pixel_width=230 and pixel_height=230
        find url examples under https://www.legacysurvey.org/dr9/description/

        Parameters
        ----------

        layer : str (default 'ls-dr9' )
          layer of survey and if data, model or residuals
          This can be hsc-dr2, galex, unwise or vlass1.2
        bands : str (default None )
          specify the bands for example grz
        file_format : str (default 'jpg' )
          format can be jpg or fits
        height : int (default 70 )
          height in arcsec
        width : int (default 70 )
          width in arcsec
        scale : int (default 262 )
          the pixel scale times 1000. this denots who many arcseonds one pixel will be

        Returns
        -------
        url : str
          url to access legacy survey image
        """
        pixscale = scale / 1000
        pixel_height = int(height / pixscale)
        pixel_width = int(width / pixscale)
        
        url = 'http://legacysurvey.org/viewer/cutout.%s?' % file_format
        url += 'ra=%.4f&dec=%.4f' % (self.ra, self.dec)
        url += '&height=%i&width=%i' % (pixel_height, pixel_width)
        url += '&layer=%s' % layer
        url += '&pixscale=%.2f' % pixscale
        if bands is not None:
            url += '&bands=%s' % bands
        return url

    def get_first_img_url(self, size=70):
        r"""

        :param size: image size in arcsec
        :param index: index
        :return:
        """

        c = SkyCoord(ra=self.ra * u.degree, dec=self.dec * u.degree)
        ra_coords = c.ra.hms
        dec_coords = c.dec.dms
        d = dec_coords.d
        if d > 0:
            d_string = '+%02i' % d
        else:
            if d == 0:
                if dec_coords.m > 0:
                    d_string = '+%02i' % d
                else:
                    if dec_coords.m == 0:
                        if dec_coords.s >= 0:
                            d_string = '+%02i' % d
                        else:
                            d_string = '-%02i' % abs(d)
                    else:
                        d_string = '-%02i' % abs(d)
            else:
                d_string = '-%02i' % abs(d)

        url_beginning = 'https://third.ucllnl.org/cgi-bin/firstimage?'
        url_coord_part = 'RA=%02i%%20%02i%%20%06.3f%%20%%2B%s%%20%02i%%20%05.2f&Dec=&Equinox=J2000&' % \
                         (ra_coords.h, abs(ra_coords.m), abs(ra_coords.s), d_string, abs(dec_coords.m),
                          abs(dec_coords.s))
        url_finish = 'ImageSize=%.2f&MaxInt=10&FITS=1&Download=1' % (size / 60)  # convert size into arcmin

        return url_beginning + url_coord_part + url_finish

    def get_lotss_dr1_img_url(self, mosaic_id, size=70):
        r"""

        :param size: image size in arcsec
        :return:
        """
        # change mosaic id to url convention
        mosaic_id = mosaic_id.replace('+', '_')

        # resize the right ascension with the declination to get squared image
        dec_size = size / 3600
        ra_size = size / 3600 / abs(np.cos(self.dec * np.pi / 180))

        url_beginning = 'https://vo.astron.nl/getproduct/hetdex/data/mosaics/%s-mosaic.fits?' % mosaic_id
        url_coord_part = 'sdec=%.3f&dec=%.5f&ra=%.5f&sra=%.3f' % (dec_size, self.dec, self.ra, ra_size)

        return url_beginning + url_coord_part

    @staticmethod
    def get_legacy_survey_img_file_name(layer='ls-dr9', bands=None, file_format='jpg', height=70, width=70, scale=262):
        """
        Parameters like in method ~get_legacy_survey_img_url
        """
        img_file_name = 'img'
        img_file_name += '_layer_%s' % layer
        img_file_name += '_height_%i' % height
        img_file_name += '_width_%i' % width
        img_file_name += '_scale_%i' % scale
        if bands is not None:
            img_file_name += '_bands_%s' % bands
        else:
            img_file_name += '_bands_none'
        return Path(img_file_name).with_suffix('.' + file_format)

    @staticmethod
    def get_radio_img_file_name(survey='first', size=70, file_format='fits'):
        return Path('radio_img_%s_size_%i' % (survey, size)).with_suffix('.' + file_format)

    def get_legacy_survey_coord_img(self, layer='ls-dr9', height=70, width=70, scale=262):

        # download legacy survey images
        url_legacy_g_band_fits = self.get_legacy_survey_img_url(layer='ls-dr9', bands='g', file_format='fits',
                                                                height=height, width=width, scale=scale)
        url_legacy_rgb_jpg = self.get_legacy_survey_img_url(layer=layer, file_format='jpg',
                                                            height=height, width=width, scale=scale)

        img_path = self.get_image_path()
        file_name_legacy_g_band_fits = self.get_legacy_survey_img_file_name(layer='ls-dr9', bands='g', file_format='fits',
                                                                        height=height, width=width, scale=scale)
        file_name_legacy_rgb_jpg = self.get_legacy_survey_img_file_name(layer=layer, file_format='jpg',
                                                                    height=height, width=width, scale=scale)

        download_file(file_path=img_path, file_name=file_name_legacy_g_band_fits, url=url_legacy_g_band_fits)
        download_file(file_path=img_path, file_name=file_name_legacy_rgb_jpg, url=url_legacy_rgb_jpg)

        g_hdu = fits.open(img_path / file_name_legacy_g_band_fits)
        g_header = g_hdu[0].header
        img_rgb = mpimg.imread(img_path / file_name_legacy_rgb_jpg)
        g_hdu.close()

        return g_header, img_rgb

    def get_radio_coord_img(self, survey='first', size=70, mosaic_id=None):
        file_path = self.get_image_path()
        file_name = self.get_radio_img_file_name(survey=survey, size=size)

        if survey == 'first':
            url = self.get_first_img_url(size=size)
        elif survey == 'lotss_dr1':
            url = self.get_lotss_dr1_img_url(mosaic_id, size=size)
        else:
            raise KeyError('radio survey should be first or lotss_dr1')

        download_file(file_path, file_name, url)

        hdu = fits.open(file_path / file_name)[0]
        header = hdu.header
        data = hdu.data

        return header, data

    def get_legacy_survey_psf(self, band='r'):
        # get description from https://www.legacysurvey.org/dr9/files/#id41

        # get survey bricks
        if self.dec < 33:
            hemispher = 'south'
        else:
            hemispher = 'north'
        survey_brick_name = 'survey-bricks-dr9-%s.fits' % (hemispher)
        survey_brick_url = ('https://portal.nersc.gov/cfs/cosmo/data/legacysurvey/'
                            'dr9/%s/survey-bricks-dr9-%s.fits.gz' % (hemispher, hemispher))

        # check if survey brick is downloaded
        if not os.path.isdir(self.data_path / 'legacy_survey'):
            os.makedirs(self.data_path / 'legacy_survey')
        if not os.path.isfile(self.data_path / 'legacy_survey' / survey_brick_name):
            download_file(file_path=self.data_path / 'legacy_survey', file_name=survey_brick_name, url=survey_brick_url, unpack=True)

        survey_bricks = fits.open(self.data_path / 'legacy_survey' / survey_brick_name)
        ra_brick = survey_bricks[1].data['RA']
        dec_brick = survey_bricks[1].data['DEC']
        psf_size_fwhm = survey_bricks[1].data['PSFSIZE_%s' % band.upper()]

        # # in case to plot the PSF
        # cm = plt.cm.get_cmap('RdYlBu')
        # sc = plt.scatter(ra_brick, dec_brick, c=psf_size_fwhm, vmin=0, vmax=3, cmap=cm)
        # plt.colorbar(sc)
        # plt.show()

        coord_table_brick = SkyCoord(ra_brick*u.deg, dec_brick*u.deg)
        coord_object = SkyCoord(self.ra*u.deg, self.dec*u.deg)
        idx_table, d2d_table, d3d_table = coord_object.match_to_catalog_sky(coord_table_brick)
        if d3d_table > 1:
            raise LookupError('the PSF is one degree far away from you destination.')

        # FWHM of psf in arcseconds
        return psf_size_fwhm[idx_table]

    def get_legacy_survey_statmorph_params(self, band='r', height=70, width=70, scale=262, show_plot=False):

        url_legacy_r_band_fits = self.get_legacy_survey_img_url(layer='ls-dr9', bands=band, file_format='fits',
                                                                height=height, width=width, scale=scale)
        img_path = self.get_image_path()

        file_name_legacy_r_band_fits = self.get_legacy_survey_img_file_name(layer='ls-dr9', bands=band, file_format='fits',
                                                                        height=height, width=width, scale=scale)

        download_file(file_path=img_path, file_name=file_name_legacy_r_band_fits, url=url_legacy_r_band_fits)
        print(url_legacy_r_band_fits)
        print(file_name_legacy_r_band_fits)

        from astropy.io import fits
        import photutils
        import scipy.ndimage as ndi
        import statmorph
        from astropy.wcs import WCS

        r_hdu = fits.open(img_path / file_name_legacy_r_band_fits)
        r_band = r_hdu[0].data
        wcs_helix = WCS(r_hdu[0].header)

        psf = self.get_legacy_survey_psf(band=band)
        print('psf (FWHM in arcsec) = ', psf)
        # change psf from FWHM to sigma
        psf /= 2.355
        # change psf from arcseconds to pixel
        print('psf (sigma in arcsec) = ', psf)
        pixel_1 = wcs_helix.world_to_pixel(SkyCoord(self.ra*u.deg, self.dec*u.deg))
        pixel_2 = wcs_helix.world_to_pixel(SkyCoord(self.ra*u.deg + psf*u.arcsec, self.dec*u.deg))
        sigma_psf_pixel = pixel_1[0] - pixel_2[0]
        print('psf (sigma in pixel scale) = ', sigma_psf_pixel)

        size = 20  # on each side from the center
        y, x = np.mgrid[-size:size+1, -size:size+1]
        psf_map = np.exp(-(x**2 + y**2)/(2.0*sigma_psf_pixel**2))
        psf_map /= np.sum(psf_map)
        # in case to calculate the
        # plt.imshow(psf, origin='lower', cmap='gray')
        # plt.show()

        # calculate the largest segment
        threshold = photutils.detect_threshold(r_band, 2.5)
        npixels = 5  # minimum number of connected pixels
        segm = photutils.detect_sources(r_band, threshold, npixels)
        # Keep only the largest segment
        label = np.argmax(segm.areas) + 1
        segmap = segm.data == label
        # # in case you want to show the segment plot
        # plt.imshow(segmap, origin='lower', cmap='gray')
        # plt.show()
        # flaten the edges
        segmap_float = ndi.uniform_filter(np.float64(segmap), size=10)
        segmap = segmap_float > 0.5
        # plt.imshow(segmap, origin='lower', cmap='gray')
        # plt.show()

        # perform statmorph fit
        source_morphs = statmorph.source_morphology(r_band, segmap, gain=4.43, psf=psf_map)
        morph = source_morphs[0]

        # if you want to show the plot
        fig = None
        if show_plot:
            from statmorph.utils.image_diagnostics import make_figure
            fig = make_figure(morph)
            # plt.show()

        # get fit parameters
        parameter_dict = {}
        # get sersic fit
        # change sersic fit from pixel to arcseconds
        sersic_rhalf_pixel = morph.sersic_rhalf
        a = wcs_helix.pixel_to_world(0, 0)
        b = wcs_helix.pixel_to_world(sersic_rhalf_pixel, 0)
        sersic_rhalf = float((a.ra - b.ra).to_string(unit=u.arcsec, decimal=True))
        # fill parameter dict
        parameter_dict.update({'sersic_amplitude': morph.sersic_amplitude})
        parameter_dict.update({'sersic_n': morph.sersic_n})
        parameter_dict.update({'sersic_rhalf': sersic_rhalf})
        parameter_dict.update({'sersic_ellip': morph.sersic_ellip})
        parameter_dict.update({'flag_sersic': morph.flag_sersic})

        # update further parameters
        parameter_dict.update({'xc_centroid': morph.xc_centroid})
        parameter_dict.update({'yc_centroid': morph.yc_centroid})
        parameter_dict.update({'ellipticity_centroid': morph.ellipticity_centroid})
        parameter_dict.update({'elongation_centroid': morph.elongation_centroid})
        parameter_dict.update({'orientation_centroid': morph.orientation_centroid})
        parameter_dict.update({'xc_asymmetry': morph.xc_asymmetry})
        parameter_dict.update({'yc_asymmetry': morph.yc_asymmetry})
        parameter_dict.update({'ellipticity_asymmetry': morph.ellipticity_asymmetry})
        parameter_dict.update({'elongation_asymmetry': morph.elongation_asymmetry})
        parameter_dict.update({'orientation_asymmetry': morph.orientation_asymmetry})
        parameter_dict.update({'rpetro_circ': morph.rpetro_circ})
        parameter_dict.update({'rpetro_ellip': morph.rpetro_ellip})
        parameter_dict.update({'rhalf_circ': morph.rhalf_circ})
        parameter_dict.update({'rhalf_ellip': morph.rhalf_ellip})
        parameter_dict.update({'r20': morph.r20})
        parameter_dict.update({'r80': morph.r80})
        parameter_dict.update({'Gini': morph.gini})
        parameter_dict.update({'M20': morph.m20})
        parameter_dict.update({'F(G, M20)': morph.gini_m20_bulge})
        parameter_dict.update({'S(G, M20)': morph.gini_m20_merger})
        parameter_dict.update({'sn_per_pixel': morph.sn_per_pixel})
        parameter_dict.update({'C': morph.concentration})
        parameter_dict.update({'A': morph.asymmetry})
        parameter_dict.update({'S': morph.smoothness})
        parameter_dict.update({'shape_asymmetry': morph.shape_asymmetry})
        parameter_dict.update({'flag': morph.flag})

        return fig, parameter_dict