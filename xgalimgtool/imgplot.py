# -*- coding: utf-8 -*-


import numpy as np

import matplotlib.pyplot as plt
import matplotlib.image as mpimg

from astropy import units as u
from astropy.wcs import WCS
from astropy.visualization.wcsaxes import SphericalCircle
from astropy.coordinates import SkyCoord

from xgalimgtool import imgtool
from xgaltool.basic_attributes import download_file

class ImgPlot:

    def __init__(self):
        r"""
        class to handle image data
        """

    @staticmethod
    def plot_coord_image(ax, header, img, title=None, x_label=True, y_label=True, fontsize=13,
                         x_label_rot=0, y_label_rot=60, dec_minpad=-0.4, ra_minpad=0.3):

        if title is not None:
            ax.set_title(title, fontsize=fontsize)
        ax.imshow(np.flipud(img), origin='lower')

        # you can customize the ticks with e.g. spacing=20 * u.arcsec
        # ax.coords['ra'].set_ticks_visible(True)
        # ax.coords['ra'].set_ticklabel_visible(True)
        # ax.coords['dec'].set_ticks_visible(True)
        # ax.coords['dec'].set_ticklabel_visible(True)

        ax.coords['ra'].set_ticks(color='white')
        ax.coords['dec'].set_ticks(color='white')
        ax.coords['ra'].display_minor_ticks(True)
        ax.coords['dec'].display_minor_ticks(True)

        # We need to set the ticklabel position explicitly
        ax.coords['ra'].set_ticklabel_position('b')
        ax.coords['dec'].set_ticklabel_position('l')


        # rotate the axis
        ax.coords['ra'].set_ticklabel(rotation=x_label_rot, exclude_overlapping=True)
        ax.coords['dec'].set_ticklabel(rotation=y_label_rot, exclude_overlapping=True)
        # grids
        # ax.coords.grid(color='grey', ls=':', linewidth=2)

        # Still refers to the image x axis
        if x_label:
            ax.coords['ra'].set_axislabel('R.A. (2000.0)', minpad=ra_minpad, fontsize=fontsize)
        else:
            ax.coords['ra'].set_axislabel(' ', minpad=ra_minpad, fontsize=fontsize)
        if y_label:
            ax.coords['dec'].set_axislabel('DEC. (2000.0)', minpad=dec_minpad, fontsize=fontsize)
        else:
            ax.coords['dec'].set_axislabel(' ', minpad=dec_minpad, fontsize=fontsize)
        ax.tick_params(axis='both', which='both', width=1.5, direction='in', color='white', labelsize=fontsize)

    @staticmethod
    def plot_coord_circle(ax, ra, dec, radius, color='g', linestyle='-', linewidth=3, alpha=1.):

        circle = SphericalCircle((ra * u.deg, dec * u.deg), radius * u.arcsec,
                                 edgecolor=color, facecolor='none', linewidth=linewidth, linestyle=linestyle,
                                 alpha=alpha, transform=ax.get_transform('icrs'))

        ax.add_patch(circle)

    @staticmethod
    def plot_long_slit(ax, wcs, ra, dec, slit_length, slit_width, angle=0, color='g', alpha=1.):

        # get unrotated part relative positions
        rel_top_left_coord_no_rot = (+ 0.5 * slit_width * u.arcsec, +  0.5 * slit_length * u.arcsec)
        rel_bottom_left_coord_no_rot = (+ 0.5 * slit_width * u.arcsec, -  0.5 * slit_length * u.arcsec)
        rel_top_right_coord_no_rot = (- 0.5 * slit_width * u.arcsec, +  0.5 * slit_length * u.arcsec)
        rel_bottom_right_coord_no_rot = (- 0.5 * slit_width * u.arcsec, -  0.5 * slit_length * u.arcsec)

        # rotate coordinates
        rel_top_left_coord = (rel_top_left_coord_no_rot[0] * np.cos(angle * np.pi / 180) +
                              rel_top_left_coord_no_rot[1] * np.sin(angle * np.pi / 180),
                              -rel_top_left_coord_no_rot[0] * np.sin(angle * np.pi / 180) +
                              rel_top_left_coord_no_rot[1] * np.cos(angle * np.pi / 180))
        rel_bottom_left_coord = (rel_bottom_left_coord_no_rot[0] * np.cos(angle * np.pi / 180) +
                              rel_bottom_left_coord_no_rot[1] * np.sin(angle * np.pi / 180),
                              -rel_bottom_left_coord_no_rot[0] * np.sin(angle * np.pi / 180) +
                              rel_bottom_left_coord_no_rot[1] * np.cos(angle * np.pi / 180))
        rel_top_right_coord = (rel_top_right_coord_no_rot[0] * np.cos(angle * np.pi / 180) +
                              rel_top_right_coord_no_rot[1] * np.sin(angle * np.pi / 180),
                              -rel_top_right_coord_no_rot[0] * np.sin(angle * np.pi / 180) +
                              rel_top_right_coord_no_rot[1] * np.cos(angle * np.pi / 180))
        rel_bottom_right_coord = (rel_bottom_right_coord_no_rot[0] * np.cos(angle * np.pi / 180) +
                              rel_bottom_right_coord_no_rot[1] * np.sin(angle * np.pi / 180),
                              -rel_bottom_right_coord_no_rot[0] * np.sin(angle * np.pi / 180) +
                              rel_bottom_right_coord_no_rot[1] * np.cos(angle * np.pi / 180))

        # add position of galaxy
        top_left_coord = SkyCoord(ra * u.deg + rel_top_left_coord[0], dec * u.deg + rel_top_left_coord[1])
        bottom_left_coord = SkyCoord(ra * u.deg + rel_bottom_left_coord[0], dec * u.deg + rel_bottom_left_coord[1])
        top_right_coord = SkyCoord(ra * u.deg + rel_top_right_coord[0], dec * u.deg + rel_top_right_coord[1])
        bottom_right_coord = SkyCoord(ra * u.deg + rel_bottom_right_coord[0], dec * u.deg + rel_bottom_right_coord[1])

        top_left_map_1_pixel_map = wcs.world_to_pixel(top_left_coord)
        bottom_left_map_1_pixel_map = wcs.world_to_pixel(bottom_left_coord)
        top_right_map_1_pixel_map = wcs.world_to_pixel(top_right_coord)
        bottom_right_map_1_pixel_map = wcs.world_to_pixel(bottom_right_coord)

        ax.plot([top_left_map_1_pixel_map[0], bottom_left_map_1_pixel_map[0]],
                      [top_left_map_1_pixel_map[1], bottom_left_map_1_pixel_map[1]], color=color, alpha=alpha)
        ax.plot([top_left_map_1_pixel_map[0], top_right_map_1_pixel_map[0]],
                      [top_left_map_1_pixel_map[1], top_right_map_1_pixel_map[1]], color=color, alpha=alpha)
        ax.plot([bottom_right_map_1_pixel_map[0], top_right_map_1_pixel_map[0]],
                      [bottom_right_map_1_pixel_map[1], top_right_map_1_pixel_map[1]], color=color, alpha=alpha)
        ax.plot([bottom_left_map_1_pixel_map[0], bottom_right_map_1_pixel_map[0]],
                      [bottom_left_map_1_pixel_map[1], bottom_right_map_1_pixel_map[1]], color=color, alpha=alpha)

    @staticmethod
    def plot_manga_fov(manga_im, ax, wcs, color='m', linestyle='-', linewidth=2, alpha=1):
        # get IFU hexagon pixel coordinates
        hexagon_pix = wcs.wcs_world2pix(manga_im.bundle.hexagon, 1)
        # reconnect the last point to the first point.
        hexagon_pix = np.concatenate((hexagon_pix, [hexagon_pix[0]]), axis=0)

        ax.plot(hexagon_pix[:, 0], hexagon_pix[:, 1],  color=color, linestyle=linestyle, linewidth=linewidth, alpha=alpha)


    @staticmethod
    def plot_obs_circle(ax, ra, dec):
        # sdss
        ImgPlot.plot_coord_circle(ax=ax, ra=ra, dec=dec, radius=3/2, color='r', linestyle='-', linewidth=3, alpha=1.)
        # co10
        ImgPlot.plot_coord_circle(ax=ax, ra=ra, dec=dec, radius=23/2, color='g', linestyle='--', linewidth=3, alpha=1.)
        # co21
        ImgPlot.plot_coord_circle(ax=ax, ra=ra, dec=dec, radius=12/2, color='y', linestyle='--', linewidth=3, alpha=1.)

class PaternPlot:
    def __init__(self):
        r"""
        Class to produce different plots for SDSS images

        """
    @staticmethod
    def image_pattern(ra, dec, figsize=None, max_pictures=20,
                      height=70, width=70, scale=262, number_in_line=10):

        number_of_rows = int(max_pictures / number_in_line)
        if figsize is None:
            figsize = (number_in_line * 4, number_of_rows * 4.8)

        # if len(table) > max_pictures:
        #     length = max_pictures
        # else:
        #     length = len(table)

        fig = plt.figure(figsize=figsize)

        for index in range(0, len(ra)):
            ax = fig.add_subplot(int(len(ra) / number_in_line) + 1, number_in_line, index + 1)

            image_object = imgtool.ImgTool(ra=ra[index], dec=dec[index])

            img_path = image_object.get_image_path()
            url_legacy_rgb_jpg = image_object.get_legacy_survey_img_url(layer='ls-dr9', file_format='jpg',
                                                            height=height, width=width, scale=scale)
            file_name_legacy_rgb_jpg = image_object.get_legacy_survey_img_file_name(layer='ls-dr9', file_format='jpg',
                                                                    height=height, width=width, scale=scale)
            download_file(file_path=img_path, file_name=file_name_legacy_rgb_jpg, url=url_legacy_rgb_jpg)

            try:
                img = mpimg.imread(img_path / file_name_legacy_rgb_jpg)
                plt.imshow(img)
            except:
                continue
            ax.axis('off')
        return fig

    @staticmethod
    def adaptive_image_pattern(ra, dec, redshift, figsize=None, max_pictures=20,
                      height=70, width=70, scale=262, number_in_line=10):

        number_of_rows = int(max_pictures / number_in_line)
        if figsize is None:
            figsize = (number_in_line * 4, number_of_rows * 4.8)

        # if len(table) > max_pictures:
        #     length = max_pictures
        # else:
        #     length = len(table)

        fig = plt.figure(figsize=figsize)

        for index in range(0, len(ra)):
            ax = fig.add_subplot(int(len(ra) / number_in_line) + 1, number_in_line, index + 1)

            image_object = imgtool.ImgTool(ra=ra[index], dec=dec[index])

            img_path = image_object.get_image_path()
            if redshift[index] > 0.1:
                url_legacy_rgb_jpg = image_object.get_legacy_survey_img_url(layer='ls-dr9', file_format='jpg',
                                                                height=height/ 2, width=width/ 2, scale=scale)
                file_name_legacy_rgb_jpg = image_object.get_legacy_survey_img_file_name(layer='ls-dr9', file_format='jpg',
                                                                        height=height/ 2, width=width/ 2, scale=scale)
            else:
                url_legacy_rgb_jpg = image_object.get_legacy_survey_img_url(layer='ls-dr9', file_format='jpg',
                                                                height=height, width=width, scale=scale)
                file_name_legacy_rgb_jpg = image_object.get_legacy_survey_img_file_name(layer='ls-dr9', file_format='jpg',
                                                                        height=height, width=width, scale=scale)

            download_file(file_path=img_path, file_name=file_name_legacy_rgb_jpg, url=url_legacy_rgb_jpg)

            try:
                img = mpimg.imread(img_path / file_name_legacy_rgb_jpg)
                plt.imshow(img)
            except:
                continue
            ax.axis('off')
        return fig